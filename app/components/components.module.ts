import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsComponent } from './news/news.component';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [NewsComponent, HeaderComponent],
  imports: [CommonModule, IonicModule],
  exports: [NewsComponent, HeaderComponent],
})
export class ComponentsModule {}
