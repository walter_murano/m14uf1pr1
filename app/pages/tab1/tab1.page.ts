import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/interfaces';
import { NewsService } from 'src/app/services/news.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  articles: Article[] = [];

  constructor(private data: NewsService) {}

  ngOnInit(): void {
    this.loadNews();
  }

  loadNews(event?) {
    this.data.getNews().subscribe((response) => {
      console.log('News', response);
      this.articles.push(...response.articles);

      if (event) {
        event.target.complete();
      }
    });
  }

  loadData(event) {
    this.data.getNextPage();
    this.loadNews(event);
  }
}
